<?php

class Scandi_Newsletter_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function saveAdditionalData(Varien_Event_Observer $observer) {
        $subscriber = $observer->getSubscriber();
        $name       = Mage::app()->getRequest()->getParam('name');

        $validator = new Zend_Validate_Alnum(array('allowWhiteSpace' => true));
        if (!$validator->isValid($name)) {
            Mage::throwException('Please enter a valid name.');
        }

        $subscriber->setName($name);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function addGridColumn(Varien_Event_Observer $observer) {
        $block = $observer->getBlock();
        if ($block && block instanceof Mage_Adminhtml_Block_Newsletter_Subscriber_Grid) {
            /** @var Mage_Adminhtml_Block_Newsletter_Subscriber_Grid $block */
            $block->addColumnAfter('name', array(
                'header'    => 'Name',
                'index'     => 'name',
            ), 'lastname');
        }
    }
}