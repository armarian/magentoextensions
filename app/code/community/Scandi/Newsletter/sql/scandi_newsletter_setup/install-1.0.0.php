<?php
/**
 * Add Name column to newsletter subscriber table
 */
    $this->getConnection()
        ->addColumn(
            $this->getTable('newsletter/subscriber'), 
            'name', 
            'varchar(50) not null'
        );